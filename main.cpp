#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    //maximized window
    //w.setWindowState(Qt::WindowMaximized);


    /*use the full screen window */
    //full screen window
    w.setWindowState(Qt::WindowFullScreen);

    w.show();

    return a.exec();
}
