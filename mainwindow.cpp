#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QDoubleValidator>
#include <QLabel>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QSslCipher>

#include <QJsonDocument>
#include <QJsonObject>

#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->lineEdit,SIGNAL(returnPressed()),this,SLOT(checkChanged()));

    ui->lineEdit->setFocus();

    //set the text color of the welcome label
    ui->welcomeLabel->setStyleSheet("QLabel {color:red; font-weight:bold;}");

    //set the text color of the status label
    ui->statusLabel->setStyleSheet("QLabel {color:blue;}");

    //set up the timer to clear the status label
    theTimer = new QTimer(this);
    theTimer->setInterval(5000); //5 seconds

    //connect(theTimer,&QTimer::timeout,this,clearStatus());
    connect(theTimer,SIGNAL(timeout()),this,SLOT(clearStatus()));

    sendPostNow = false;
    number = "";

    manager = new QNetworkAccessManager();
    reply = Q_NULLPTR;

    QSslConfiguration config = QSslConfiguration::defaultConfiguration();

//    QList<QSslCipher> list = config.supportedCiphers();
//    foreach (QSslCipher item, list)
//    {
//        qDebug()<<item.name();
//    }
    request.setSslConfiguration(config);
    request.setUrl(QUrl("https://intraweb.wvutech.edu/cardreader.php"));

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(replyFinished(QNetworkReply*)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::checkChanged()
{
    qDebug()<<"inside checkchanged"<<ui->lineEdit->text();

    number = cleanNumber(ui->lineEdit->text());
    sendPostNow = true;
    reply = manager->get(request);
    connect(reply,SIGNAL(metaDataChanged()),this,SLOT(getCode()));

    ui->lineEdit->setText("");
    ui->lineEdit->setFocus();
}

void MainWindow::replyFinished(QNetworkReply *r)
{
    //this gets the web page itself
    QString incoming = r->readAll();
    qDebug()<<"the page:" << incoming;

    QJsonDocument doc = QJsonDocument::fromJson(incoming.toUtf8());

    QJsonObject obj;
    if (!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
        }
    }
    QString f = obj["first_name"].toString();
    QString l = obj["last_name"].toString();
    if (f.length() > 0 && l.length()>0)
    {
        ui->statusLabel->setText("Thank you for using the lab!");
        //start the timer before the label gets cleared
        theTimer->start();
        qDebug()<<"the timer should be started";
    }
    if (sendPostNow)
    {
        sendPostData();
        sendPostNow = false;
    }
    r->deleteLater();
}


void MainWindow::getPage()
{
    number = ui->lineEdit->text();
    sendPostNow = true;
    reply = manager->get(request);
    connect(reply,SIGNAL(metaDataChanged()),this,SLOT(getCode()));

    ui->statusLabel->setText("");
    ui->lineEdit->setText("");
    ui->lineEdit->setFocus();
}

void MainWindow::getCode()
{
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    qDebug()<<"status code: " <<statusCode;
}

void MainWindow::sendPostData()
{
    QUrlQuery query;
    query.addQueryItem("param1","username");
    query.addQueryItem("param2","bigpassword");

    query.addQueryItem("number",number);


    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    reply = manager->post(request,query.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply,SIGNAL(metaDataChanged()),this,SLOT(getCode()));

    qDebug()<<"we just now posted";
    number = "";
}


QString MainWindow::cleanNumber(QString s)
{
//;123456789=1115?
//    +E?
    QString fixed = "";
    QStringList list = s.split('=');
    if (list.size() > 0)
    {
        QString first = list[0];
        if (first.length() >0)
        {
            if (first.startsWith(';'))
            {
                //remove the first character
                fixed = first.remove(0,1);
            }
            else
            {
                fixed = first;
            }
        }
    }
    return fixed;
}

void MainWindow::clearStatus()
{
    //qDebug()<<"should be here now";
    //empty the status text when the timer goes off
    ui->statusLabel->setText("");
    theTimer->stop();
}
