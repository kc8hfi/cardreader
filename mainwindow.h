#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkRequest>

class QNetworkAccessManager;
class QNetworkReply;
class QTimer;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

    void sendPostData();


private slots:
    void checkChanged ();
    void replyFinished(QNetworkReply *);
    void getPage();
    void getCode();
    void clearStatus();

private:
    Ui::MainWindow *ui;

    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;

    //cleans up the data coming from a card swipe
    QString cleanNumber(QString);

    bool sendPostNow;

    QString number;
    //QSslConfiguration config = QSqlConfiguration::defaultConfiguration();

    QTimer *theTimer;



};

#endif // MAINWINDOW_H
